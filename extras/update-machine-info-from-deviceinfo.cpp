#include <cstdlib>
#include <string>

#include <glib.h>
#include <gio/gio.h>

#include <deviceinfo.h>

int main() {
    DeviceInfo info;
    bool has_error = false;
    g_autoptr(GError) error = NULL;

    g_autoptr(GDBusProxy) hostname1 = g_dbus_proxy_new_for_bus_sync(
        G_BUS_TYPE_SYSTEM,
        G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS,
        /* interface info */ NULL,
        "org.freedesktop.hostname1",
        "/org/freedesktop/hostname1",
        "org.freedesktop.hostname1",
        /* cancellable */ NULL,
        &error);

    if (!hostname1) {
        g_error("Can't connect to org.freedesktop.hostname1, can't update machine info (%s)",
            error->message);
        return EXIT_FAILURE;
    }

    g_autoptr(GVariant) pretty_hostname_v = g_dbus_proxy_get_cached_property(hostname1, "PrettyHostname");
    const char *pretty_hostname = NULL;
    if (pretty_hostname_v)
        pretty_hostname = g_variant_get_string(pretty_hostname_v, /* (out) length */ NULL);

    g_autoptr(GVariant) chassis_v = g_dbus_proxy_get_cached_property(hostname1, "Chassis");
    const char *chassis = NULL;
    if (chassis_v)
        chassis = g_variant_get_string(chassis_v, /* (out) length */ NULL);

    g_debug("From org.freedesktop.hostname1, PrettyHostname = '%s', Chassis = '%s'",
        pretty_hostname ?: "(NULL)",
        chassis ?: "(NULL)");

    /* Only update hostname when pretty hostname is not present yet
     * as otherwise it could have been overriden by the user */
    if (!pretty_hostname || pretty_hostname[0] == '\0') {
        std::string pretty_name = info.prettyName();

        g_autoptr(GVariant) ret = g_dbus_proxy_call_sync(
            hostname1,
            "SetPrettyHostname",
            g_variant_new("(sb)", pretty_name.c_str(), /* interactive */ FALSE),
            G_DBUS_CALL_FLAGS_NONE,
            /* timeout_msec */ 1000,
            /* cancellable */ NULL,
            &error);

        if (error) {
            g_warning("Can't set pretty hostname: %s", error->message);
            g_clear_error(&error);

            has_error = true;
        } else {
            g_debug("Set pretty hostname to '%s'", pretty_name.c_str());
        }
    }

    switch (info.deviceType()) {
        case DeviceInfo::DeviceType::Phone:
            chassis = "handset";
            break;
        case DeviceInfo::DeviceType::Tablet:
            chassis = "tablet";
            break;
        case DeviceInfo::DeviceType::Desktop:
        case DeviceInfo::DeviceType::Unknown:
            /* In these cases Hostnamed usually knows better. */
            chassis = NULL;
            break;
    }

    if (chassis) {
        g_autoptr(GVariant) ret = g_dbus_proxy_call_sync(
            hostname1,
            "SetChassis",
            g_variant_new("(sb)", chassis, /* interactive */ FALSE),
            G_DBUS_CALL_FLAGS_NONE,
            /* timeout_msec */ 1000,
            /* cancellable */ NULL,
            &error);

        if (error) {
            g_warning("Can't set chassis: %s", error->message);
            g_clear_error(&error);

            has_error = true;
        } else {
            g_debug("Set chassis to '%s'", chassis);
        }
    }

    return has_error ? EXIT_FAILURE : EXIT_SUCCESS;
}
