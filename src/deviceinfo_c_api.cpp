#include <cstdlib>
#include <cstring>
#include <iostream>

#include <deviceinfo.h>

#ifdef __cplusplus
extern "C" {
#endif

DeviceInfo* deviceinfo_new() {
	return new DeviceInfo();
}

void deviceinfo_delete(DeviceInfo* instance) {
	delete instance;
}

static char* to_cstr(std::string& input) {
	char* cstr = strdup(input.c_str());
	return cstr;
}

// libdeviceinfo function declarations

char* deviceinfo_name(DeviceInfo* instance) {
	std::string str = instance->name();
	return to_cstr(str);
}

char* deviceinfo_prettyName(DeviceInfo* instance) {
	std::string str = instance->prettyName();
	return to_cstr(str);
}

int deviceinfo_gridUnit(DeviceInfo* instance) {
	return instance->gridUnit();
}

bool deviceinfo_contains(DeviceInfo* instance, const char* prop) {
	return instance->contains(prop);
}

char* deviceinfo_get(DeviceInfo* instance, const char* prop, const char* defaultValue) {
	std::string str = instance->get(prop, defaultValue);
	return to_cstr(str);
}

char* deviceinfo_supportedOrientations(DeviceInfo* instance) {
	std::string str = instance->get("SupportedOrientations", "Portrait,InvertedPortrait,Landscape,InvertedLandscape");
	return to_cstr(str);
}

char* deviceinfo_primaryOrientation(DeviceInfo* instance) {
	std::string str = instance->primaryOrientation();
	return to_cstr(str);
}

char* deviceinfo_portraitOrientation(DeviceInfo* instance) {
	std::string str = instance->portraitOrientation();
	return to_cstr(str);
}

char* deviceinfo_invertedPortraitOrientation(DeviceInfo* instance) {
	std::string str = instance->invertedPortraitOrientation();
	return to_cstr(str);
}

char* deviceinfo_landscapeOrientation(DeviceInfo* instance) {
	std::string str = instance->landscapeOrientation();
	return to_cstr(str);
}

char* deviceinfo_invertedLandscapeOrientation(DeviceInfo* instance) {
	std::string str = instance->invertedLandscapeOrientation();
	return to_cstr(str);
}

int deviceinfo_deviceType(DeviceInfo* instance) {
	return instance->deviceType();
}

int deviceinfo_driverType(DeviceInfo* instance) {
	return instance->driverType();
}

#ifdef __cplusplus
} // extern "C"
#endif
